#include <AccelStepper.h>
#include <MIDI.h>
#include <MultiStepper.h>

// Define pin connections for steppers

#define stepPin_1 4
#define dirPin_1 7

#define stepPin_2 3
#define dirPin_2 6

#define stepPin_3 2
#define dirPin_3 5

#define stepPin_4 12
#define dirPin_4 13

// Define the number of motors
const int numMotors = 4;

// Create an array of pointers to AccelStepper objects
AccelStepper* steppers[numMotors];

MIDI_CREATE_DEFAULT_INSTANCE();
void handleNoteOn(byte inChannel, byte inNote, byte inVelocity);
void handleNoteOff(byte inChannel, byte inNote, byte inVelocity);

bool direction = true;

float getFrequency(byte midiNote);

void setup() {

    // Initialize each stepper object
    steppers[0] = new AccelStepper(AccelStepper::DRIVER, stepPin_1, dirPin_1);
    steppers[1] = new AccelStepper(AccelStepper::DRIVER, stepPin_2, dirPin_2);
    steppers[2] = new AccelStepper(AccelStepper::DRIVER, stepPin_3, dirPin_3);
    steppers[3] = new AccelStepper(AccelStepper::DRIVER, stepPin_4, dirPin_4);

    for (int i = 0; i < numMotors; i++) {
      steppers[i]->setMaxSpeed(3000);
    }

    Serial.begin(31250);
    Serial.println("Serial Active!");

    MIDI.setHandleNoteOn(handleNoteOn);
    MIDI.setHandleNoteOff(handleNoteOff);
    MIDI.begin(MIDI_CHANNEL_OMNI);

}

void loop() {


  MIDI.read(); 

  for (int i = 0; i < numMotors; i++){
    steppers[i]->runSpeed();
  }
}

// Converts MIDI note number into frequency
float getFrequency(byte midiNote){
  return 440.0 * pow(2, (midiNote - 69) / 12.0);
}


// Callback function for note on
void handleNoteOn(byte inChannel, byte inNote, byte inVelocity)
{

  // // Arduino Controlling channels 1 - 4
  // if (inChannel <= 4){
  //   steppers[inChannel - 1]->setSpeed(getFrequency(inNote));
  // }

  // Arduino Controlling channels 4 - 8
  if (inChannel > 4){
    steppers[(inChannel - 4) - 1]->setSpeed(getFrequency(inNote));
  }


  
}


// Callback function for note off
void handleNoteOff(byte inChannel, byte inNote, byte inVelocity)
{

  // // Arduino Controlling channels 1 - 4
  // if (inChannel <= 4){
  //   steppers[inChannel - 1]->setSpeed(0); 
  // }

  // Arduino Controlling channels 4 - 8
  if (inChannel > 4){  
    steppers[(inChannel - 4) - 1]->setSpeed(0);
  }
  
 
}

