# Stepper Motor Band - Motorious B.O.T

## Description
This project is heavily inspired from Jonathon Kayne's https://github.com/jzkmath/MIDI-Stepper-Synth-V2 but as his version utilises an FGPA,  im trying to stick with a software focused approach by using an Arduino(As I am a Software Engineering Student).

I've always loved playing instruments, I was looking for a way to combine my interests of Embedded Software and Music, so when I saw the above project I was immediately inspired and decided to give it a go myself. 

## Stepper Motors
This project aims to take advantage of the musical qualities of seemingly non-musical items. 
In this case we are using Stepper Motors; these are typically used in 3D printers and CNC machines.

![Stepper Motor Diagram](images/What-is-a-Stepper-Motor%20(1).jpg)

We control a stepper motors movement by sending an electrical pulse to the STEP pin on its dedicated motor driver; A single pulse will cause the motor to ‘step’ once, so we can control the speed of our motor by altering the frequency in which we send electrical signals to the driver. 

## Making Music
In simple terms a musical note is a frequency of vibrations, for example a guitar string that vibrates 440 times a second will produce a frequency of 440Hz which is the musical note A at the fourth octave. 

![Piano Roll](images/music_kbd_2-300.jpg) ![frequency chart](images/music6.jpg) 

## Putting it all together
It just so happens that stepper motors vibrate when they move,  we can control the frequency of ‘steps’  the motor takes , which means we can control the frequency of the vibration that the motor produces. 

So, sending 440 electrical pulses to our motor a second will cause it to step 440 times in that second and therefore vibrate at a frequency of 440Hz which is our A4 note. 

Great! What now? Well, we need a way to communicate to the motors what note(frequency) they should be playing if any. For this we are using two Arduino Uno’s connected to a MIDI interface. 
MIDI  (Musical Instrument Digital Interface) is the standard way of communicating musical notes electronically, with every note frequency having a matching MIDI note number.

![Midi note chart](images/note-names-frequencies-in-Hz-and-MIDI-note-numbers-the-boldface-number-stands-for-the.png)

For our use case, all we need our Arduinos to do is receive this note number and convert it into a frequency of electrical signals to be sent to the required motor. 

The only MIDI data we are using for this project is Channel (Determines what motor data is being sent to) and Note (determines the speed of motor and thus vibration/musical note).
Using this set up we can either play pre-made midi tracks from the laptop or connect a MIDI instrument such as a keyboard.

## Diagram Example
![Set up Diagram](images/Stepper%20Diagram.jpg)

## Some example songs

https://www.youtube.com/watch?v=SIEKhEUoWPY (Eleanor Rigby) V.2 

## Using as MIDI instrument with guitar
https://www.linkedin.com/feed/update/urn:li:activity:7265995955445936128/


## Images

# Initial Set Up
![Image One](img/PXL_20240501_145239758.jpg)

# Current Set Up 
![Image Two](img/PXL_20240627_103803417.jpg)

## Equipment used
2 x Arduino Uno

8x A4988 Motor Drivers 

8x RS Pro 12v Stepper Motors (Any Nema 17 style steppers will work)

2x CNC Expansion Shield (Holds motor drivers for easier set up, less wires etc.)

1x MIDI Expansion Shield (Converts MIDI messages into a form that Arduinos can process)

1x USB MIDI Interface (Connects laptop to MIDI shield to send data)

DAW Software (E.G reaper, for processing MIDI)

Bench Power Supply (Allows for powering of all components)


## Future Plans
I'm interested in developing this project further by extending the amount of motors used, and experimenting with the acoustic engineering aspect. 